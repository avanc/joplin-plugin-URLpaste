import joplin from 'api';
import { ContentScriptType, SettingItemType } from 'api/types';

joplin.plugins.register({
  onStart: async function() {
    console.info('Starting Plugin');
    await joplin.contentScripts.register(
      ContentScriptType.CodeMirrorPlugin,
      'URLpaste',
      './URLPastePlugin.js'
    )
  }
});
