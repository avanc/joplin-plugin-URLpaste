module.exports = {
  default: function(context: any) {


    const plugin = function(CodeMirror) {
      console.info("Register CM plugin")
      CodeMirror.defineOption('URLPaste', false, function(cm, value, prev) {
        if (!value) return;
        console.info("Register events");
        cm.on('inputRead', async function (cm1, event) {
          console.log(event)
        });

        cm.on('paste', async function (cm1, event) {
          console.info("Paste event")
          console.log(event)
        });

        cm.on('scroll', async function (cm1, event) {
          console.info("Scroll event")
          console.log(event)
        });

        cm.on('keydown', async function (cm1, event) {
          console.info("Key event")
          console.log(event)
        });

        cm.on('copy', async function (cm1, event) {
          console.info("Copy event")
          console.log(event)
        });        
      });
    };

    return {
      plugin: plugin,
      codeMirrorResources: [
      ],
      codeMirrorOptions: {
        'URLPaste': true,
      }
        }
    }
}
